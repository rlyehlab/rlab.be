# R'lyeh hacklab

Sitio principal del hacklab. Publicado en https://rlab.be


## Cómo usar el sitio

* Instalar jekyll: https://jekyllrb.com/docs/installation/

* Buildear la aplicación

```
$ bundle install
```

* Correr jekyll en modo servidor

```
$ jekyll serve
```

