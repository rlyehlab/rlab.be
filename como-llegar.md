---
layout: home
title: Cómo llegar
permalink: /como-llegar/
group: navigation
nav_text: "Cómo llegar"
nav_order: 2
---

El lab esta ubicado en Uruguay y Bartolomé Mitre en la Ciudad Autónoma de
Buenos Aires. Es un espacio de aprendizaje, construcción y trabajo horizontal,
podés venir a trabajar en tus proyectos en cualquier momento que esté
[abierto](https://wiki.rlab.be/books/sobre-el-hacklab/page/%C2%BFcu%C3%A1ndo-abre) o a cualquiera de
las actividades. Si no tenes la direccion, [pedinosla por algún medio](/contacto).

Tanto el ingreso al lab como las actividades son gratuitas. Funcionamos con un
sistema de donaciones, así que si querés ayudarnos a que el espacio siga abierto
podes hacerlo con [bonos mensuales o donaciones esporádicas](/donaciones).

Si querés venir a socializar, la mejor idea es que lo hagas uno de los días
específicos para eso. La gente que viene a trabajar puede que este ocupada y no
pueda responderte de manera extensa, no tomes esto como algo malo :) Revisá
nuestras [redes sociales](/contacto) para estar al tanto de las últimas
novedades.

También podés revisar la [Agenda](https://agenda.rlab.be/) para enterarte sobre
las actividades programadas.

