---
layout: home
group: navigation
nav_text: "Inicio"
nav_order: 1
---

R'lyeh es un [hacklab](https://endefensadelsl.org/hacklabs-y-hackerspaces.html)
que funciona en la Ciudad Autónoma de Buenos Aires.

Es un espacio físico para hackear, aprender a hackear, enseñar a hackear, crear,
romper, arreglar y desarrollar. Hackear no es solo software, lo entendemos y
extendemos a todas las áreas.

Somos una comunidad organizada entre pares que entiende a la tecnología como un
fenómeno social, histórico, político y económico. Se promueve la mutualización
entre personas y organizaciones para generar nuevos tipos de relaciones no
basadas en la competencia y se defiende el uso y desarrollo de software libre y
anti capitalista.

Queremos que el hacklab sea un espacio seguro, por eso te pedimos que antes de venir
leas el [Código de Conducta y Antiacoso](https://wiki.rlab.be/books/sobre-el-hacklab/page/c%C3%B3digo-de-conducta-y-antiacoso).
Es importante que lo respetes y lo hagas respetar. Si ves alguna situación de
conflicto o algo te incomoda en el espacio, tenemos un
[protocolo de resolución de conflictos](https://wiki.rlab.be/books/sobre-el-hacklab/page/resoluci%C3%B3n-de-conflictos)
para resolver estas situaciones y que no se repitan.

